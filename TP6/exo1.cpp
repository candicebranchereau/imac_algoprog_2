#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
    for (int i = 0; i < nodeCount; i++)
    {
        GraphNode* newNode = new GraphNode(i);
        this->appendNewNode(newNode);
    }

    for (int i = 0; i < nodeCount; i++)
    {
        for (int j = 0; j < nodeCount; j++)
        {
            if (adjacencies[i][j] >= 1)
                this->nodes[i]->appendNewEdge(nodes[j], adjacencies[i][j]);
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
    visited[first->value] = true;
    nodes[nodesSize] = first;
    nodesSize++;

    Edge *e = first->edges;
    while (e != NULL)
    {
        if(!visited[e->destination->value])
            deepTravel(e->destination, nodes, nodesSize, visited);

        e = e->next;
    }
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */

    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);

    while (!nodeQueue.empty())
    {
        nodes[nodesSize] = nodeQueue.front();
        nodesSize ++;
        visited[nodeQueue.front()->value] = true;

        Edge *e = nodeQueue.front()->edges;
        while (e != NULL)
        {
            if(!visited[e->destination->value])
                nodeQueue.push(e->destination);

            e = e->next;
        }
        nodeQueue.pop();
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    visited[first->value] = true;

    Edge *e = first->edges;
    while (e != NULL)
    {
        if (visited[e->destination->value])
            return true;
        else
            detectCycle(e->destination, visited);

        e = e->next;
    }
    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
