#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier != NULL)
        return false;
    else
        return true;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* NouvelElement = (Noeud *)malloc(sizeof(Noeud));
    NouvelElement->donnee = valeur;
    NouvelElement->suivant = NULL;

    if (est_vide(liste)) //si notre liste est vide on initialise le premier élément
    {
        liste->premier = NouvelElement;
    }
    else //sinon on ajoute l'élément à la suite
    {
        Noeud* actuel = liste->premier;

        while(actuel->suivant != NULL)
        {
            actuel = actuel->suivant;
        }

        actuel->suivant = NouvelElement;
    }
}

void affiche(const Liste* liste)
{
    Noeud* actuel = liste->premier;

    while(actuel != NULL)
    {
        cout << actuel->donnee << " ";
        actuel = actuel->suivant;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    Noeud* actuel = liste->premier;

    for (int i=0; i<n; i++)
    {
        actuel = actuel->suivant;
    }
    return actuel->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* actuel = liste->premier;
    int index = 0;

    while(actuel->suivant != NULL)
    {
        if (actuel->donnee == valeur)
            return index;

        actuel = actuel->suivant;
        index++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* actuel = liste->premier;

    for (int i=0; i<n; i++)
    {
        actuel = actuel->suivant;
    }

    actuel->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->taille == tableau->capacite)
    {
        tableau->capacite++;
        tableau->donnees = (int*) realloc(tableau->donnees,sizeof(int)*tableau->capacite);
    }

    tableau->donnees[tableau->taille] = valeur;
    tableau->taille++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees = (int*) malloc(sizeof(int)*capacite);
    tableau->taille = 0;
    tableau->capacite = capacite;
}

bool est_vide(const DynaTableau* tableau)
{
    for (int i=0; i<tableau->taille; i++)
    {
        if (tableau->donnees[i] != NULL)
            return false;
    }
    return true;
}

void affiche(const DynaTableau* tableau)
{
    for (int i=0; i<tableau->taille; i++)
    {
        cout << tableau->donnees[i] << " " ;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i=0; i<tableau->taille; i++)
    {
        if (tableau->donnees[i] == valeur)
            return i;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

void pousse_file(DynaTableau* tableau, int valeur) //Ajoute (à la fin)
{
    ajoute(tableau, valeur);
}
/*void pousse_file(Liste* liste, int valeur) //Ajoute (à la fin)
{
    ajoute(liste, valeur);
}*/

int retire_file(DynaTableau* tableau) //Retire le premier élément ajouté (au début)
{
    int DonneeSup = tableau->donnees[0];
    for(int i=0; i<tableau->taille; i++)
    {
        tableau->donnees[i] = tableau->donnees[i+1];
    }
    tableau->taille--;
    return DonneeSup;
}
/*int retire_file(Liste* liste) //Retire le premier élément ajouté (au début)
{
    int DonneeSup = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return DonneeSup;
}*/

void pousse_pile(DynaTableau* tableau, int valeur) //Ajoute (au début)
{
    if (tableau->taille == tableau->capacite)
    {
        tableau->capacite++;
        tableau->donnees = (int*) realloc(tableau->donnees,sizeof(int)*tableau->capacite);
    }

    for(int i=tableau->taille; i>=0; i--)
    {
        tableau->donnees[i+1] = tableau->donnees[i];
    }
    tableau->donnees[0] = valeur;
    tableau->taille++;
}
/*
void pousse_pile(Liste* liste, int valeur) //Ajoute (à la fin)
{
    ajoute(liste, valeur);
}*/

int retire_pile(DynaTableau* tableau) //Retire le dernier élément ajouté (au début)
{
    int DonneeSup = tableau->donnees[0];
    for(int i=0; i<tableau->taille; i++)
    {
        tableau->donnees[i] = tableau->donnees[i+1];
    }
    tableau->taille--;
    return DonneeSup;
}
/*int retire_pile(Liste* liste) //Retire le dernier élément ajouté (à la fin)
{
    Noeud* actuel = liste->premier;
    int DonneeSup;

    if (actuel->suivant == NULL)
    {
        DonneeSup = actuel->donnee;
        liste->premier = NULL;
    }
    else
    {
        while(actuel->suivant->suivant != NULL)
        {
            actuel = actuel->suivant;
        }
        DonneeSup = actuel->suivant->donnee;
        actuel->suivant = actuel->suivant->suivant;
    }
    return DonneeSup;
}*/


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste a " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste a " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apres stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    //Liste pile;
    DynaTableau pile;
    //Liste file;
    DynaTableau file;

    initialise(&pile, 7);
    initialise(&file, 7);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis la..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis la..." << std::endl;
    }

    return 0;
}
